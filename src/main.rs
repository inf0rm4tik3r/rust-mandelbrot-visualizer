use std::env;
use std::fs;

use log::error;
use num::Complex;
use num::complex::Complex64;
use pixels::{PixelsBuilder, SurfaceTexture, wgpu};
use rayon::prelude::*;
use winit::{
    event::Event,
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};
use winit::dpi::PhysicalSize;
use winit::event::VirtualKeyCode;
use winit_input_helper::WinitInputHelper;

mod renderer;
mod mandelbrot;
mod complex;
mod string;
mod timing;

const DEFAULT_RES_X: usize = 1280;
const DEFAULT_RES_Y: usize = 720;

fn main() {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("WARN")).init();
    let mut state = create_state();

    let threads = num_cpus::get();
    rayon::ThreadPoolBuilder::new().num_threads(threads).build_global().unwrap();

    let mut greyscale_map = GreyscaleMap::new(state.resolution);
    create_mandelbrot_image(&state, &mut greyscale_map);

    let event_loop = EventLoop::new();
    let mut input = WinitInputHelper::new();
    let window = {
        let size = PhysicalSize::new(state.resolution.x as f64, state.resolution.y as f64);
        WindowBuilder::new()
            .with_title("Mandelbrot")
            .with_inner_size(size)
            //.with_min_inner_size(size)
            //.with_max_inner_size(size)
            .build(&event_loop)
            .unwrap()
    };

    let mut pixels = {
        let window_size = window.inner_size();
        let surface_texture = SurfaceTexture::new(
            window_size.width, window_size.height, &window);
        match PixelsBuilder::new(
            state.resolution.x as u32, state.resolution.y as u32, surface_texture)
            .request_adapter_options(wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::HighPerformance,
                compatible_surface: None,
            })
            .enable_vsync(false)
            .build() {
            Ok(pixels) => pixels,
            Err(e) => panic!("Unable to crate Pixels: {:?}", e)
        }
    };

    event_loop.run(move |event, _, control_flow| {
        // Draw the current frame
        if let Event::RedrawRequested(_) = event {
            blit_image_r(&mut greyscale_map.pixels, pixels.get_frame());
            if pixels
                .render()
                .map_err(|e| error!("pixels.render() failed: {:?}", e))
                .is_err()
            {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }

        // Handle input events
        if input.update(&event) {
            // Close events
            if input.key_pressed(VirtualKeyCode::Escape) || input.quit() {
                *control_flow = ControlFlow::Exit;
                return;
            }
            let move_strength = 0.05;
            let zoom_strength = 0.1;
            let mut changed = false;
            if input.key_held(VirtualKeyCode::Up) {
                state.up(move_strength);
                changed = true;
            }
            if input.key_held(VirtualKeyCode::Down) {
                state.down(move_strength);
                changed = true;
            }
            if input.key_held(VirtualKeyCode::Left) {
                state.left(move_strength);
                changed = true;
            }
            if input.key_held(VirtualKeyCode::Right) {
                state.right(move_strength);
                changed = true;
            }
            if input.key_held(VirtualKeyCode::Space) {
                state.zoom_in(zoom_strength);
                changed = true;
            }
            if input.key_held(VirtualKeyCode::LShift) {
                state.zoom_out(zoom_strength);
                changed = true;
            }

            if let Some(size) = input.window_resized() {
                pixels.resize_surface(size.width, size.height);
                changed = true;
            }

            if changed {
                create_mandelbrot_image(&state, &mut greyscale_map);
                window.request_redraw();
            }
        }
    });
}

fn blit_image_r(image: &mut [u8], frame: &mut [u8]) {
    // Frame has 4 times more bytes, as frame is RGBA, image is just R!
    for (i, pixel) in frame.chunks_exact_mut(4).enumerate() {
        let mandelbrot_grey = image[i];
        let rgba = [mandelbrot_grey, mandelbrot_grey, mandelbrot_grey, 0xff];
        pixel.copy_from_slice(&rgba);
    }
}

fn create_mandelbrot_image(options: &State, greyscale_map: &mut GreyscaleMap) {
    timing::measure_time("rendering", || {
        //render(&mut pixels, resolution, (upper_left, lower_right))
        render_threaded_rayon(
            greyscale_map,
            complex::complex_plane_at(
                options.center, options.zoom, options.resolution.aspect_ratio(),
            ),
        )
    });
}

fn write_mandelbrot_image_to_file(options: &State, greyscale_map: &GreyscaleMap) {
    let file = match fs::File::create(&options.filename) {
        Ok(file) => file,
        Err(e) => panic!("Unable to open output file! {}", e)
    };
    match image::codecs::png::PngEncoder::new(file).encode(
        &greyscale_map.pixels,
        options.resolution.x as u32,
        options.resolution.y as u32,
        image::ColorType::L8,
    ) {
        Ok(_) => (),
        Err(e) => panic!("Unable to write image: {}", e)
    }
}

struct State {
    resolution: Resolution,
    center: Complex64,
    zoom: f64,
    filename: String,
}

impl State {
    fn up(&mut self, strength: f64) {
        self.center.im -= strength * 1.0 / self.zoom;
    }
    fn down(&mut self, strength: f64) {
        self.center.im += strength * 1.0 / self.zoom;
    }
    fn left(&mut self, strength: f64) {
        self.center.re -= strength * 1.0 / self.zoom;
    }
    fn right(&mut self, strength: f64) {
        self.center.re += strength * 1.0 / self.zoom;
    }
    fn zoom_in(&mut self, strength: f64) {
        self.zoom = self.zoom + strength * self.zoom;
    }
    fn zoom_out(&mut self, strength: f64) {
        self.zoom = self.zoom - strength * self.zoom;
    }
}

fn create_state() -> State {
    let args: Vec<String> = env::args().collect();
    if args.len() != 5 {
        print_usage(&args);
        println!("Using default arguments..");
        let resolution = Resolution::new(DEFAULT_RES_X, DEFAULT_RES_Y);
        let center = Complex { re: -0.5, im: -0.6 };
        let zoom = 15.0;
        return State {
            resolution,
            center,
            zoom,
            filename: "target\\mandel.png".to_string(),
        };
    }
    State {
        resolution: Resolution::from_tuple(
            string::parse_pair(&args[2], 'x')
                .expect("Error parsing resolution parameter.")
        ),
        center: complex::parse_complex(&args[3]).expect("Error parsing center parameter."),
        zoom: args[4].parse::<f64>().unwrap(),
        filename: args[1].clone(),
    }
}

fn print_usage(args: &[String]) {
    eprintln!("Usage: {} FILE RESOLUTION UPPER_LEFT LOWER_RIGHT", args[0]);
    eprintln!("Example: {} mandel.png 1920x1080 -1.20,0.35 -1,0.2", args[0]);
}

struct GreyscaleMap {
    pixels: Vec<u8>,
    resolution: Resolution,
}

impl GreyscaleMap {
    fn new(resolution: Resolution) -> GreyscaleMap {
        GreyscaleMap {
            pixels: vec![0_u8; resolution.x * resolution.y],
            resolution,
        }
    }
}

fn render_threaded_rayon(
    greyscale_map: &mut GreyscaleMap,
    complex_plane: (Complex64, Complex64),
) {
    let amount_of_bands = greyscale_map.resolution.y;
    let bands = create_bands(amount_of_bands, &mut greyscale_map.pixels, greyscale_map.resolution, complex_plane);
    bands.into_par_iter().for_each(|band| {
        render(band.pixels, band.resolution, band.complex_plane);
    });
}

fn render_threaded_crossbeam(
    pixels: &mut [u8],
    resolution: Resolution,
    complex_plane: (Complex64, Complex64),
) {
    let threads = num_cpus::get() * 2;
    let bands = create_bands(threads, pixels, resolution, complex_plane);
    crossbeam::scope(|spawner| {
        for band in bands {
            spawner.spawn(move |_| {
                timing::measure_time("rendering band", || {
                    render(band.pixels, band.resolution, band.complex_plane);
                });
            });
        }
    }).unwrap();
}

fn create_bands(
    amount_of_bands: usize,
    pixels: &mut [u8],
    resolution: Resolution,
    complex_plane: (Complex64, Complex64),
) -> Vec<Band> {
    let rows_per_band = resolution.y / amount_of_bands + 1;
    pixels.chunks_mut(rows_per_band * resolution.x).enumerate().map(
        |(i, pixel_band)| {
            let top = rows_per_band * i;
            let height = pixel_band.len() / resolution.x;
            let band_res = Resolution::new(resolution.x, height);
            let upper_left = complex::pixel_to_point(resolution, (0, top), complex_plane);
            let lower_right = complex::pixel_to_point(resolution, (resolution.x, top + height), complex_plane);
            // println!("{}, {}, {}x{}, {},{}", top, height, band_res.x, band_res.y, upper_left, lower_right);
            Band {
                resolution: band_res,
                complex_plane: (upper_left, lower_right),
                pixels: pixel_band,
            }
        }
    ).collect()
}

struct Band<'a> {
    resolution: Resolution,
    complex_plane: (Complex64, Complex64),
    pixels: &'a mut [u8],
}

fn render(
    pixels: &mut [u8],
    resolution: Resolution,
    complex_plane: (Complex64, Complex64),
) {
    assert_eq!(pixels.len(), resolution.x * resolution.y);
    for row in 0..resolution.y {
        for column in 0..resolution.x {
            let pixel = (column, row);
            let point = complex::pixel_to_point(resolution, pixel, complex_plane);
            let index = row * resolution.x + column;
            pixels[index] = match mandelbrot::escape_time(point, 255) {
                // Part of the Mandelbrot set:
                None => 0,
                // Not part of the set:
                Some(iterations) => 255 - iterations as u8
            };
        }
    }
}

#[derive(Clone, Copy)]
pub struct Resolution {
    x: usize,
    y: usize,
}

impl Resolution {
    fn new(x: usize, y: usize) -> Self {
        Resolution { x, y }
    }
    fn from_tuple(tuple: (usize, usize)) -> Self {
        Resolution { x: tuple.0, y: tuple.1 }
    }
    fn aspect_ratio(self) -> f64 {
        self.x as f64 / self.y as f64
    }
}
