use num::complex::Complex64;
use num::Complex;
use crate::{Resolution, string};

/// `zoom` must be >= 1!
pub fn complex_plane_at(center: Complex64, zoom: f64, asr: f64) -> (Complex64, Complex64) {
    // (Complex64 { re: -1.2, im: 0.35 }, Complex64 { re: -1.0, im: 0.2 })
    let inv_zoom = 1.0 / zoom;
    let zoom_factor = 2.0;
    let re_offset = (inv_zoom * zoom_factor).abs();
    let re_ul = center.re - re_offset;
    let re_lr = center.re + re_offset;
    let im_offset = (inv_zoom * zoom_factor).abs();
    let im_ul = center.im - im_offset / asr;
    let im_lr = center.im + im_offset / asr;
    let c = (Complex64 { re: re_ul, im: im_ul }, Complex64 { re: re_lr, im: im_lr });
    println!("center + zoom: {}, {}", center, zoom);
    //println!("complex_plane_at: {:?}", c);
    c
}

/// Given a `pixel`s row and column, return the corresponding point on the complex plane.
/// `resolution` contains the dimension (width and height) of the mage in pixels.
/// `pixel` contains the row and column, indication a particular pixel-location in that image.
/// The `upper_left` and `lower_right` parameters are points on the complex plane designating the
/// area our image covers.
pub fn pixel_to_point(
    resolution: Resolution,
    pixel: (usize, usize),
    complex_plane: (Complex64, Complex64),
) -> Complex64 {
    let upper_left = complex_plane.0;
    let lower_right = complex_plane.1;
    let (complex_width, complex_height) = (
        lower_right.re - upper_left.re,
        upper_left.im - lower_right.im
    );
    let (res_x, res_y) = (resolution.x as f64, resolution.y as f64);
    let (pix_x, pix_y) = (pixel.0 as f64, pixel.1 as f64);
    Complex {
        re: upper_left.re + pix_x * complex_width / res_x,
        im: upper_left.im - pix_y * complex_height / res_y,
        // Why subtraction here?
        // pix_y increases as we go down, but the imaginary component increases as we go up.
    }
}

#[test]
fn test_pixel_to_point() {
    assert_eq!(
        pixel_to_point(
            Resolution { x: 100, y: 200 },
            (25, 175),
            (Complex { re: -1.0, im: 1.0 }, Complex { re: 1.0, im: -1.0 }),
        ),
        Complex { re: -0.5, im: -0.75 }
    );
}

/// Parses a pair of floating-point numbers separated by a comma as a complex number.
pub fn parse_complex(string: &str) -> Option<Complex64> {
    string::parse_pair(string, ',').map(|(re, im)| Complex { re, im })
}

#[test]
fn test_parse_complex() {
    assert_eq!(parse_complex("1.25,-0.0625"), Some(Complex { re: 1.25, im: -0.0625 }))
}
