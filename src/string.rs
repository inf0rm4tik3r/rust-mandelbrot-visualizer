use std::str::FromStr;

/// Parses the string `s` as being two elements, separated by the given `separator`.
/// The string is assumed to look like: "`<left><separator><right>`".
/// Both `<left>` and `<right>` must be representable as `T` by calling `T::from_str`.
pub fn parse_pair<T: FromStr>(string: &str, separator: char) -> Option<(T, T)> {
    match string.find(separator) {
        None => None,
        Some(index) => {
            match (T::from_str(&string[..index]),
                   T::from_str(&string[index + 1..])) {
                (Ok(left), Ok(right)) => Some((left, right)),
                _ => None
            }
        }
    }
}

#[test]
fn test_parse_pair() {
    assert_eq!(parse_pair::<i32>("", ','), None);
    assert_eq!(parse_pair::<i32>("10", ','), None);
    assert_eq!(parse_pair::<i32>(",10", ','), None);
    assert_eq!(parse_pair::<i32>("10,20x", ','), None);
    assert_eq!(parse_pair::<i32>("10,20", ','), Some((10, 20)));
    assert_eq!(parse_pair::<f64>("1.25,-0.0625", ','), Some((1.25, -0.0625)));
    assert_eq!(parse_pair::<f64>("0.5x", 'x'), None);
    assert_eq!(parse_pair::<f64>("0.5x1.5", 'x'), Some((0.5, 1.5)));
}
