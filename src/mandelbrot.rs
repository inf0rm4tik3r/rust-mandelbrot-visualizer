use num::complex::Complex64;

/// Try to determine if `c` is in the Mandelbrot set, using at most `limit` iterations to decide.
/// Returns `Some(i)` if `c` is not a member, where `i` is the number of iterations it took for `c`
/// to leave the circle of radius centered on the origin.
/// Returns `None` if `c` seems to be a member (more precisely, if we reached the iteration limit
/// without being able to proof that `c` is not a member).
pub fn escape_time(complex: Complex64, limit: usize) -> Option<usize> {
    let mut z = Complex64 { re: 0.0, im: 0.0 };
    for i in 0..limit {
        // Check if complex number escaped the circle with radius 2.
        // If it does, it would later approach infinity and is therefore not in the mandelbrot set.
        if z.norm_sqr() > 4.0 {
            return Some(i);
        }
        // Otherwise iterate further.
        z = z * z + complex;
    }
    // The complex number did not leave the circle after limit iterations.
    // Therefore assume that is it part of the set.
    None
}
