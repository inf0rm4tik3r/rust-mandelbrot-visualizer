use std::time::Duration;

pub fn measure_time<F: FnMut()>(what: &str, mut fun: F) {
    let start = std::time::Instant::now();
    fun();
    let end = std::time::Instant::now();
    println!("{} took: {} seconds", what, duration_to_seconds(&(end - start)));
}

fn duration_to_seconds(duration: &Duration) -> f64 {
    duration.as_secs() as f64 + duration.subsec_nanos() as f64 * 1e-9
}
