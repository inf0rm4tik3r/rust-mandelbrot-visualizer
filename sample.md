    let mut last_percentage_shown = 0.0;
    let mut percentage = 0.0;
    
    percentage = (row * resolution.x) as f64 / (resolution.x * resolution.y) as f64 * 100.0;
    if (percentage - last_percentage_shown) > 1.0 {
        //println!("{}%", percentage.floor());
        last_percentage_shown = percentage;
    }
